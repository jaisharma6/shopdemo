﻿using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;
using UnityEngine.UI;

public class MenuItem : MonoBehaviour {
    #region PrivateFields
    [SerializeField]
    private VRInteractiveItem m_interactiveItem; //VR interactive item script of the gameobject
    [SerializeField]
    private SelectionRadial m_selectionRadial; //Selection Radial
    [SerializeField]
    private Shadow m_shadow; //Shadow component of the gameobject
    [SerializeField]
    private InteractionManager m_interactionManager; //Interaction manager of the scene

    [Space]
    [SerializeField]
    private GameObject menuCanvas; //Menu GameObject
    [SerializeField]
    private Transform productHolder; //Parent gameobject of all the productss
    [SerializeField]
    private Transform cube; //Cube prefab
    [SerializeField]
    private List<Transform> spawnPoints = new List<Transform>(); //List of all the spawn points

    private bool isSelected = false; //True if the item is selected else false
    #endregion

    #region UnityBuiltIns
    // Use this for initialization
    void Start () {
		
	}

    /// <summary>
    /// Called whenever this script is enabled
    /// Registers all the methods to the delegates
    /// </summary>
    private void OnEnable()
    {
        m_interactiveItem.OnOver += OnHoverEnter;
        m_interactiveItem.OnOut += OnHoverExit;
        m_selectionRadial.OnSelectionComplete += OnItemSelect;
    }

    // Update is called once per frame
    void Update () {
		
	}

    /// <summary>
    /// Called whenever this script is disabled
    /// UnRegisters all the methods from the delegates
    /// </summary>
    private void OnDisable()
    {
        m_interactiveItem.OnOver -= OnHoverEnter;
        m_interactiveItem.OnOut -= OnHoverExit;
        m_selectionRadial.OnSelectionComplete -= OnItemSelect;
    }
    #endregion

    #region PrivateMethods

    /// <summary>
    ///  When a menu item is in focus
    ///  This method shows the selection radial
    ///  and Toggles the state of the menu item toselected
    /// </summary>
    private void OnHoverEnter()
    {
        m_selectionRadial.Show();
        ToggleSelectionState(true);
    }

    /// <summary>
    /// When focus is removed from a menu item
    /// This method hides the selection radial
    /// and Toggles the state of the menu item to not selected
    /// </summary>
    private void OnHoverExit()
    {
        m_selectionRadial.Hide();
        ToggleSelectionState();
    }

    /// <summary>
    /// Called whenever a menu item is selected
    /// </summary>
    private void OnItemSelect()
    {
        SetupShop();
    }

    /// <summary>
    /// Initializes the shop
    /// </summary>
    private void SetupShop()
    {
        if (isSelected)
        {
            m_selectionRadial.Hide(); //Hides the selection radial
            menuCanvas.SetActive(false); //Hides the menu
            PlaceProducts(); //Places the products on spawn points
            m_interactionManager.enabled = true; //enables item interaction
        }
    }

    /// <summary>
    /// Place a product on every spawn point
    /// </summary>
    private void PlaceProducts()
    {
        foreach (Transform spawnPoint in spawnPoints)
        {
            Instantiate(cube, spawnPoint.position, Quaternion.identity, productHolder);
        }
    }

    /// <summary>
    /// Toggles the state of the menu Item
    /// </summary>
    /// <param name="flag">if true Item is selected else not</param>
    private void ToggleSelectionState(bool flag = false)
    {
        m_shadow.enabled = flag;
        isSelected = flag;
    }
    #endregion
}
