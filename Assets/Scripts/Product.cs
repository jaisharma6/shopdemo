﻿using UnityEngine;

public class Product : MonoBehaviour {
    #region PrivateFields
    [SerializeField]
    private float m_rotateSpeed = 50f; //Rotation speed of the transform
    #endregion

    #region PublicFields
    [HideInInspector]
    public bool isSelected = false; //true if the product is selected else false
    [HideInInspector]
    public bool isRotating = false; //true if the item is rotating else false
    #endregion

    #region UnityBuiltIns
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //Rotate the object if isRotating is true
        if (isRotating && isSelected)
        {
            transform.RotateAround(transform.position, transform.up, Time.deltaTime * m_rotateSpeed);
        }
	}
    #endregion
}
