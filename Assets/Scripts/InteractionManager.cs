﻿using UnityEngine;
using VRStandardAssets.Utils;
using UnityEngine.SceneManagement;

public class InteractionManager : MonoBehaviour
{

    #region PrivateFields
    [SerializeField]
    private VRInput m_input; //VRInput manager script
    [SerializeField]
    private SelectionRadial m_selectionRadial; //Selection Radial
    [SerializeField]
    private Transform m_camera; //camera transform for raycast
    [SerializeField]
    private Transform selectionPosition; //Position of the selected Product

    #region NonSerialized
    private RaycastHit hit; //currently focused gameobject
    private Vector3 previousProductPosition; //Previously selected product position
    private Transform previousProduct; //Previously selected product
    #endregion
    #endregion

    #region UnityBuiltIns
    // Use this for initialization
    void Start()
    {
        if (!m_camera)
            m_camera = Camera.main.transform;
    }

    /// <summary>
    /// Called whenever this script is enabled
    /// Registers all the methods with delegates
    /// </summary>
    private void OnEnable()
    {
        m_selectionRadial.OnSelectionComplete += OnItemSelect;
        m_input.OnDoubleClick += RotateItem;
    }

    // Update is called once per frame
    void Update()
    {
        #region Raycast
        Physics.Raycast(m_camera.position, m_camera.forward, out hit);
        ToggleSelectionRadial();
        #endregion
        #region Reset
        if (Input.GetButtonDown("Cancel"))
        {
            if (!previousProduct)
                ResetScene();
            else
                ResetPreviousProduct();
        }
        #endregion
    }

    /// <summary>
    /// Called whenever this script is disabled
    /// Un-Registers all the methods from delegates
    /// </summary>
    private void OnDisable()
    {
        m_selectionRadial.OnSelectionComplete -= OnItemSelect;
        m_input.OnDoubleClick -= RotateItem;
    }
    #endregion

    #region PrivateMethods

    /// <summary>
    /// Called whenever a new Product is selected
    /// </summary>
    private void OnItemSelect()
    {
        if (hit.collider)
        {
            if (hit.collider.GetComponent<Product>())
            {
                var product = hit.collider.GetComponent<Product>();
                if (!product.isSelected)
                {
                    if (previousProduct != hit.transform && previousProduct)
                        ResetPreviousProduct();
                    previousProduct = hit.transform;
                    previousProductPosition = hit.transform.position;
                    hit.transform.position = selectionPosition.position;
                    product.isSelected = true;
                }
            }
        }
    }

    /// <summary>
    /// Starts rotating a selected Items
    /// </summary>
    private void RotateItem()
    {
        if (hit.collider)
        {
            if (hit.collider.GetComponent<Product>())
            {
                var product = hit.collider.GetComponent<Product>();
                product.isRotating = !product.isRotating;
            }
        }
    }

    /// <summary>
    /// Resets the previous product if a new product is selected
    /// </summary>
    private void ResetPreviousProduct()
    {
        if (previousProduct)
        {
            previousProduct.position = previousProductPosition;
            previousProduct.rotation = Quaternion.identity;
            var p_product = previousProduct.GetComponent<Product>();
            p_product.isSelected = false;
            p_product.isRotating = false;
            previousProduct = null;
        }
    }

    /// <summary>
    /// Resets the scene if no product is selected and Cancel button is pressed
    /// </summary>
    private void ResetScene()
    {
        SceneManager.LoadScene("MainScene"); //Loads the main scene again
    }

    /// <summary>
    /// Toggles the state of selection radial to show if it's over any product
    /// else hidden
    /// </summary>
    private void ToggleSelectionRadial()
    {
        if (hit.collider)
        {
            if (hit.collider.GetComponent<Product>())
            {
                m_selectionRadial.Show();
            }

            else
            {
                m_selectionRadial.Hide();
            }
        }

        else
        {
            m_selectionRadial.Hide();
        }
    }
    #endregion
}
